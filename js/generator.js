function generate_frame() {
	nb_frame = $("#frame_number").val();
	name_frame = $("#frame_name").val();

	$.ajax({
		url: "generator/ajax_generate_frame.php",
		type: 'POST',
		data: ({project: name_frame, nbre_frame: nb_frame})
	}).done(function(result){
		if (result == 0) {
			alert("error");
		} else if (result == 1) {
			alert("ok");
		} else if (result == 2) {
			alert("projet deja existant");
		}
		else {
			alert("error inconnu");
		}
	})
	document.getElementById("frame_number").disabled="true";
	document.getElementById("frame_name").disabled="true";
	document.getElementById("validator_state_1").disabled="true";

}