<!DOCTYPE html>
<html>
<head>
	<title>Générateur</title>
	<meta charset='utf-8'>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>

	<script type="text/javascript" src="js/_init.js"></script>
	<script type="text/javascript" src="js/generator.js"></script>
</head>
<body>
	<section id="nav-header">
		<div class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#">Project name</a>
	        </div>
	        <div id="navbar" class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li class="active"><a href="#">Home</a></li>
	            <li><a href="#about">About</a></li>
	            <li><a href="#contact">Contact</a></li>
	          </ul>
	        </div>
	      </div>
	    </div>
	</section>
	
	<section class="col-sm-2" id="option-left">
		<div class="panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Titre option</h3>
			</div>
			<div class="panel-body"><button class="btn btn-sm btn-info" onclick="show_option(1)">étape 1</button>
				<div class="option_1 list-group" id="option-secondaire">
					<select id="frame_number" class="list-group-item">
						<?php 
							for($i=0; $i <= 20; $i++)
								echo "<option value='$i'>$i</option>";
						?>
					</select>
					<input type="text" id="frame_name" placeholder="nom du projet" class="list-group-item">
					<button id="validator_state_1" class="btn btn-sm btn-success" type="button" onclick="generate_frame()">Valider</button>
				</div>
			</div>
			<div class="panel-body"><button class="btn btn-sm btn-info" onclick="show_option(2)">option 2</button>
				<div class="option_2 list-group" id="option-secondaire">
				<a class="list-group-item">item1</a>
					<a class="list-group-item">item2</a>
				</div>
			</div>
			<div class="panel-body"><button class="btn btn-sm btn-info" onclick="show_option(3)">option 3</button>
				<div class="option_3 list-group" id="option-secondaire">
				<a class="list-group-item">item1</a>
					<a class="list-group-item">item2</a>
				</div>
			</div>
			<div class="panel-body"><button class="btn btn-sm btn-info" onclick="show_option(4)">option 4</button>
				<div class="option_4 list-group" id="option-secondaire">
				<a class="list-group-item">item1</a>
					<a class="list-group-item">item2</a>
				</div>
			</div>
			<div class="panel-body"><button class="btn btn-sm btn-info" onclick="show_option(5)">option 5</button>
				<div class="option_5 list-group" id="option-secondaire">
				<a class="list-group-item">item1</a>
					<a class="list-group-item">item2</a>
				</div>
			</div>
			<div class="panel-body"><button class="btn btn-sm btn-info" onclick="show_option(6)">option 6</button>
				<div class="option_6 list-group" id="option-secondaire">
				<a class="list-group-item">item1</a>
					<a class="list-group-item">item2</a>
				</div>
			</div>
			<div class="panel-body"><button class="btn btn-sm btn-info" onclick="show_option(7)">option 7</button>
				<div class="option_7 list-group" id="option-secondaire">
				<a class="list-group-item">item1</a>
					<a class="list-group-item">item2</a>
				</div>
			</div>
		</div>
	</section>

</div>
</body>
</html>